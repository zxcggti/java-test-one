package com.yizhi.student.service.impl;

import com.yizhi.student.dao.StudentInfoDao;
import com.yizhi.student.domain.StudentInfoDO;
import com.yizhi.student.service.StudentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class StudentInfoServiceImpl implements StudentInfoService {


    @Autowired
    private StudentInfoDao studentInfoDao;


    @Override
    public StudentInfoDO get(Integer id) {
        System.out.println("======service层中传递过来的id参数是：" + id + "======");
        return studentInfoDao.get(id);
    }


    @Override
    public List<StudentInfoDO> list(Map<String, Object> map) {
        // 默认值设置
        int defaultCurrPage = 1; // 默认当前页
        int defaultPageSize = 10; // 默认页大小

        // 安全地从 map 中获取 currPage 和 pageSize
        int currPage = safeParseInt(map.get("currPage"), defaultCurrPage);
        int pageSize = safeParseInt(map.get("pageSize"), defaultPageSize);

        // 计算起始行索引
        currPage = Math.max(currPage, 1); // 确保页码为正数
        pageSize = Math.max(pageSize, 1); // 确保页面大小为正数
        int startIndex = (currPage - 1) * pageSize;

        // 更新 map 中的分页参数
        map.put("currPage", startIndex);

        return studentInfoDao.list(map);
    }


    private int safeParseInt(Object value, int defaultValue) {
        if (value instanceof String) {
            try {
                return Integer.parseInt((String) value);
            } catch (NumberFormatException e) {
                // 日志记录
            }
        }
        return defaultValue;
    }



    @Override
    public int count(Map<String, Object> map) {
        return studentInfoDao.count(map);
    }


    @Override
    public int save(StudentInfoDO studentInfo) {
        return studentInfoDao.save(studentInfo);
    }

    @Override
    public int update(StudentInfoDO studentInfo) {
        return studentInfoDao.update(studentInfo);
    }

    @Override
    public int remove(Integer id) {
        return studentInfoDao.remove(id);
    }

    @Override
    public int batchRemove(Integer[] ids) {
        return studentInfoDao.batchRemove(ids);
    }


}
